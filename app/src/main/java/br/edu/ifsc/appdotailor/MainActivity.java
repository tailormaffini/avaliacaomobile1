package br.edu.ifsc.appdotailor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void abreIMC(View view) {
        Intent intent = new Intent(this, calculaIMC.class);
        startActivity(intent);
    }

    public void abreTemp(View view) {
        Intent intent = new Intent(this, calculaTemp.class);
        startActivity(intent);
    }

}
