package br.edu.ifsc.appdotailor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class calculaIMC extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcula_imc);
    }

    public void calcularIMC(View view) {
        EditText peso = (EditText) findViewById(R.id.peso);
        EditText altura = (EditText) findViewById(R.id.altura);
        TextView resultado = (TextView) findViewById(R.id.resultado);

        float pesoNum = Float.valueOf(peso.getText().toString());
        float alturaNum = Float.valueOf(altura.getText().toString());
        float resultadoNum = pesoNum / (alturaNum * alturaNum);

        resultado.setText(String.format("%.2f", resultadoNum));



    }
}
