package br.edu.ifsc.appdotailor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class calculaTemp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcula_temp);
    }

    public void toCel(View view) {
        EditText temp = (EditText) findViewById(R.id.temperatura);
        TextView result = (TextView) findViewById(R.id.resultado);

        float tempF = Float.valueOf(temp.getText().toString());
        float tempC = Float.valueOf((float) ((tempF - 32) / 1.8));

        result.setText(String.format("%.2f", tempC) + " Cº");
    }

    public void toFah(View view) {
        EditText temp = (EditText) findViewById(R.id.temperatura);
        TextView result = (TextView) findViewById(R.id.resultado);

        float tempC = Float.valueOf(temp.getText().toString());
        float tempF = Float.valueOf((float) ((tempC * 1.8) + 32));

        result.setText(String.format("%.2f", tempF) + " Fº");
    }
}
